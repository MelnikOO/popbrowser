//
//  Media.h
//  PopBrowse
//
//  Created by Олег Мельник on 14.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Media : NSManagedObject

@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSString * caption;
@property (nonatomic, retain) NSDate * dateCreated;
@property (nonatomic, retain) NSString * fullURL;
@property (nonatomic, retain) NSNumber * likes;
@property (nonatomic, retain) NSString * previewURL;
@property (nonatomic, retain) NSString * instagramId;
@property (nonatomic, retain) NSDate * dateAdded;

@end
