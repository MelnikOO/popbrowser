//
//  Media.m
//  PopBrowse
//
//  Created by Олег Мельник on 14.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "Media.h"


@implementation Media

@dynamic author;
@dynamic caption;
@dynamic dateCreated;
@dynamic fullURL;
@dynamic likes;
@dynamic previewURL;
@dynamic instagramId;
@dynamic dateAdded;

@end
