//
//  FavoriteCell.m
//  PopBrowse
//
//  Created by Олег Мельник on 13.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "FavoriteCell.h"
#import "DataManager.h"

@implementation FavoriteCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)removeFav:(id)sender {
    NSError *error;
    [[DataManager sharedInstance] removeFromFavorites:_instagramId
                                                error:&error];
    if (!error) {
        NSLog(@"remove successful");
    }
    else {
        NSLog(@"remove error: %@", error.localizedDescription);
    }
}
@end
