//
//  FirstViewController.m
//  PopBrowse
//
//  Created by Олег Мельник on 06.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "PopularViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "PopularMediaCell.h"
#import "PopularMediaPreview.h"
#import "Media.h"
#import "DataManager.h"

@interface PopularViewController ()

@end

@implementation PopularViewController {
    SDWebImageManager *manager; // image loading and caching manager
    NSManagedObjectContext *context;
    NSMutableArray *mediaArray;
}

#pragma mark - Internal methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpTableView];
    manager = [SDWebImageManager sharedManager];
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    context = [delegate managedObjectContext];
    [[DataManager sharedInstance] addObserver:self
                  forKeyPath:@"favorites"
                     options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                     context:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"favorites"]) {
        if (!self.view.window)
            [self.tableView reloadData];
    }
}

- (void)setUpTableView {
    self.tableView.estimatedRowHeight = 240.f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    __weak UITableView *__tableView = self.tableView;
    [[DataManager sharedInstance] loadPopularWithSuccess:^(NSArray *favoritesArray) {
        mediaArray = [favoritesArray mutableCopy];
        [__tableView reloadData];
    } failure:^(NSError *error) {
        NSLog(@"error getting favorites: %@", error.localizedDescription);
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ceil(mediaArray.count / 2);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"popularCellIdentifier";
    PopularMediaCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PopularMediaCell class])
                                             owner:self
                                           options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSInteger previewsCount = cell.popularPreviews.count;
        
        // configure 1 or more elements on the cell using outlet collection
        for (NSInteger i = 0; i < previewsCount; i++) {
            if (mediaArray.count > indexPath.row * previewsCount + i) {
                [cell.popularPreviews[i] configureFromDictionary:
                 mediaArray[indexPath.row * previewsCount + i]];
            }
        }
        [cell updateButtonsState];
        
        // get more favorites if we scroll down to the end of table
        if (indexPath.row == [tableView numberOfRowsInSection:0] - 1) {
            __weak UITableView *__tableView = self.tableView;
            [[DataManager sharedInstance] loadPopularWithSuccess:^(NSArray *popularArray) {
                [mediaArray addObjectsFromArray:popularArray];
                [__tableView reloadData];
            } failure:^(NSError *error) {
                NSLog(@"error getting favorites: %@", error.localizedDescription);
            }];
        }
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
