//
//  SecondViewController.m
//  PopBrowse
//
//  Created by Олег Мельник on 06.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "FavoritesViewController.h"
#import "DataManager.h"
#import "FavoriteCell.h"
#import "Media.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface FavoritesViewController ()

@end

@implementation FavoritesViewController

#pragma mark - Internal methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpTableView];
    DataManager *dataManager = [DataManager sharedInstance];
    [dataManager addObserver:self
                  forKeyPath:@"favorites"
                     options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                     context:nil];
}

- (void)setUpTableView {
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"favorites"]) {
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DataManager *dataManager = [DataManager sharedInstance];
    return [dataManager.favorites count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 360.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"favoriteCellIdentifier";
    FavoriteCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FavoriteCell class])
                                              owner:self
                                            options:nil] firstObject];
        __weak FavoriteCell *__cell = cell;
        Media *media = ((Media *)([[DataManager sharedInstance] favorites][indexPath.row]));
        [[SDImageCache sharedImageCache] queryDiskCacheForKey:media.instagramId
                                    done:^(UIImage *image, SDImageCacheType cacheType) {
                                        if (!image) {
                                            [[SDWebImageDownloader sharedDownloader]
                                             downloadImageWithURL:[NSURL URLWithString:media.fullURL]
                                             options:0
                                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {}
                                             completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished){
                                                 if (image && finished) {
                                                     [[SDImageCache sharedImageCache] storeImage:image forKey:media.instagramId toDisk:YES];
                                                     NSLog(@"successfully download and saved");
                                                 }
                                                 else {
                                                     NSLog(@"downloading and saving failed: %@", error.localizedDescription);
                                                 }
                                                 __cell.pictureView.image = image;
                                             }];
                                        }
                                        else {
                                            __cell.pictureView.image = image;
                                        }
                                    }];
        cell.authorLabel.text = [NSString stringWithFormat:@"@%@", media.author];
        cell.likeLabel.text = [NSString stringWithFormat:@"💜%ld", [media.likes longValue]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.instagramId = media.instagramId;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
