//
//  PopularMediaCell.h
//  PopBrowse
//
//  Created by Олег Мельник on 08.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopularMediaPreview.h"

@interface PopularMediaCell : UITableViewCell
@property (strong, nonatomic) IBOutletCollection(PopularMediaPreview) NSArray *popularPreviews;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *addFavButtons;

- (IBAction)favPressed:(id)sender;
- (void)updateButtonsState;

@end
