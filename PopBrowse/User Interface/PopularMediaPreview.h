//
//  PopularMediaPreview.h
//  PopBrowse
//
//  Created by Олег Мельник on 08.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopularMediaPreview : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *likesLabel;
@property (weak, nonatomic) NSDictionary *mediaDictionary;

- (void)configureFromDictionary:(NSDictionary *)aMediaDictionary;

@end
