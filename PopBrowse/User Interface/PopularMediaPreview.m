//
//  PopularMediaPreview.m
//  PopBrowse
//
//  Created by Олег Мельник on 08.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "PopularMediaPreview.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <UIActivityIndicator-for-SDWebImage/UIImageView+UIActivityIndicatorForSDWebImage.h>

@implementation PopularMediaPreview

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        if (self.subviews.count == 0) {
            UIView *subview = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                                  owner:self
                                                options:nil] lastObject];
            subview.frame = self.bounds;
            [self addSubview:subview];
            self.imageView.image = [UIImage imageNamed:@"lena"];
        }
    }
    return self;
}

- (void)configureFromDictionary:(NSDictionary *)aMediaDictionary {
    _mediaDictionary = aMediaDictionary;
    [_imageView setImageWithURL:[NSURL URLWithString:aMediaDictionary[kMediaPreviewURL]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _authorLabel.text = [NSString stringWithFormat:@"@%@", aMediaDictionary[kMediaAuthor]];
    _likesLabel.text = [NSString stringWithFormat:@"💜%ld", [aMediaDictionary[kMediaLikes] longValue]];
}

@end
