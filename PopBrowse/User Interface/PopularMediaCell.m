//
//  PopularMediaCell.m
//  PopBrowse
//
//  Created by Олег Мельник on 08.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "PopularMediaCell.h"
#import "AppDelegate.h"
#import "Media.h"
#import "DataManager.h"

@implementation PopularMediaCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)favPressed:(id)sender {
    NSLog(@"favPressed");
    UIButton *button = sender;
    PopularMediaPreview *preview = [_popularPreviews objectAtIndex:[_addFavButtons indexOfObject:button]];
    NSError *error;
    if (![[DataManager sharedInstance] addToFavorites:preview.mediaDictionary
                                                error:&error]) {
        NSLog(@"error adding to favs: %@", error.localizedDescription);
    }
    else {
        NSLog(@"successfully added to favorites");
    }
    [self updateButtonsState];
}

- (void)updateButtonsState {
    for (PopularMediaPreview *preview in _popularPreviews) {
        UIButton *addFavButton = [_addFavButtons objectAtIndex:[_popularPreviews indexOfObject:preview]];
        NSString *instagramId = preview.mediaDictionary[kMediaInstagramId];
        if ([[DataManager sharedInstance] idExistInFavorites:instagramId]) {
            [addFavButton setTitle:@"Added" forState:UIControlStateNormal];
            addFavButton.userInteractionEnabled = NO;
        }
        else {
            [addFavButton setTitle:@"+Favorites" forState:UIControlStateNormal];
            addFavButton.userInteractionEnabled = YES;
        }
    }
}

@end
