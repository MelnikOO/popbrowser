//
//  DataLoader.h
//  PopBrowse
//
//  Created by Олег Мельник on 06.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataManager : NSObject <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSArray *favorites;

+ (id)sharedInstance; // get a singleton
- (void)loadPopularWithSuccess:(void(^)(NSArray *popularArray))successBlock
                       failure:(void(^)(NSError *error))failureBlock;
- (BOOL)addToFavorites:(NSDictionary *)mediaDictionary error:(NSError **)error;
- (BOOL)idExistInFavorites:(NSString *)instagramId;
- (BOOL)removeFromFavorites:(NSString *)instagramId error:(NSError **)error;

@end
