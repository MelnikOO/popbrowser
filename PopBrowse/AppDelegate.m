//
//  AppDelegate.m
//  PopBrowse
//
//  Created by Олег Мельник on 06.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "AppDelegate.h"
#import "Media.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _managedObjectContext = [self setUpCoreData];
    [self setUpFRC];
    // Override point for customization after application launch.
    return YES;
}

- (void)setUpFRC {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:NSStringFromClass([Media class])
                                      inManagedObjectContext:_managedObjectContext];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"dateAdded" ascending:NO];
    fetchRequest.sortDescriptors = @[sort];
    _fetchedAllResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                      managedObjectContext:_managedObjectContext
                                                                        sectionNameKeyPath:nil
                                                                                 cacheName:@"Root"];
    _fetchedAllResultsController.delegate = self;
}

- (NSManagedObjectContext *)setUpCoreData {
    NSString *bundlePath = [NSBundle mainBundle].bundlePath;
    NSURL *modelURL = [NSURL fileURLWithPath:[bundlePath stringByAppendingString:@"/model.momd"] isDirectory:NO];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    NSURL *storeURL = [NSURL fileURLWithPath:[bundlePath stringByAppendingString:@"/store.sqlite"] isDirectory:NO];
    NSError *error;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_managedObjectModel];
    [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                             configuration:nil
                                                       URL:storeURL
                                                   options:nil
                                                     error:&error];
    if (error) {
        NSLog(@"error adding persistent store: %@", error.userInfo);
        return nil;
    }
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] init];
    context.persistentStoreCoordinator = _persistentStoreCoordinator;
    return context;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
