//
//  Constants.h
//  PopBrowse
//
//  Created by Олег Мельник on 06.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#ifndef PopBrowse_Constants_h
#define PopBrowse_Constants_h

#define kInstagramClientId @"b36ac1db689a4217a8ffe722c0bd3fe5"

#define kMediaAuthor @"author"
#define kMediaCaption @"caption"
#define kMediaDateCreated @"dateCreated"
#define kMediaLikes @"likes"
#define kMediaFullURL @"fullURL"
#define kMediaPreviewURL @"previewURL"
#define kMediaInstagramId @"instagramId"

#define kAddedNewFavorite @"AddedNewFavorite"

#endif
