//
//  DataLoader.m
//  PopBrowse
//
//  Created by Олег Мельник on 06.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import "DataManager.h"
#import "AppDelegate.h"
#import "Media.h"
#import <AFNetworking/AFNetworking.h>

@implementation DataManager {
    NSFetchedResultsController *fetchedAllResultsController;
}

- (id)init {
    self = [super init];
    if (self) {
        fetchedAllResultsController = [(AppDelegate *)[[UIApplication sharedApplication] delegate] fetchedAllResultsController];
        [self fetchFavorites];
    }
    return self;
}

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static DataManager *sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)loadPopularWithSuccess:(void (^)(NSArray *))successBlock
                       failure:(void (^)(NSError *))failureBlock {
    NSString *URLString = [NSString stringWithFormat:@"%@%@",
                           @"https://api.instagram.com/v1/media/popular?client_id=",
                           kInstagramClientId];
    NSURL *URL = [NSURL URLWithString:URLString];
    NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:URLRequest];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    __weak DataManager *__self = self;
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"successfully get data");
        NSArray *popularArray = [__self parseJsonData:responseObject[@"data"]];
        NSLog(@"successfully parsed");
        successBlock(popularArray);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureBlock(error);
    }];
    [operation start];
}

- (NSArray *)parseJsonData:(NSArray *)aData {
    NSMutableArray *popularArray = [NSMutableArray array];
    for (NSDictionary *mediaDictionary in aData) { // parse incoming json to managed objs
        if ([mediaDictionary[@"type"] isEqualToString:@"image"]) {
            NSMutableDictionary *media = [@{} mutableCopy];
            [media setObject:mediaDictionary[@"user"][@"username"] forKey:kMediaAuthor];
            id caption = mediaDictionary[@"caption"];
            if (![caption isKindOfClass:[NSNull class]]) {
                [media setObject:mediaDictionary[@"caption"][@"text"] forKey:kMediaCaption];
            }
            else {
                [media setObject:@"" forKey:kMediaCaption];
            }
            [media setObject:[NSDate dateWithTimeIntervalSince1970:[mediaDictionary[@"created_time"] integerValue]] forKey:kMediaDateCreated];
            [media setObject:mediaDictionary[@"images"][@"standard_resolution"][@"url"] forKey:kMediaFullURL];
            [media setObject:mediaDictionary[@"likes"][@"count"] forKey:kMediaLikes];
            [media setObject:mediaDictionary[@"images"][@"low_resolution"][@"url"] forKey:kMediaPreviewURL];
            [media setObject:mediaDictionary[@"id"] forKey:kMediaInstagramId];
            [popularArray addObject:media];
        }
    }
    return [popularArray copy];
}

- (BOOL)addToFavorites:(NSDictionary *)mediaDictionary error:(NSError *__autoreleasing *)error {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = delegate.managedObjectContext;
    Media *mediaToAdd = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Media class])
                                                      inManagedObjectContext:context];
    if (!mediaToAdd) {
        return NO;
    }
    mediaToAdd.author = mediaDictionary[kMediaAuthor];
    mediaToAdd.caption = mediaDictionary[kMediaCaption];
    mediaToAdd.dateCreated = mediaDictionary[kMediaDateCreated];
    mediaToAdd.likes = mediaDictionary[kMediaLikes];
    mediaToAdd.fullURL = mediaDictionary[kMediaFullURL];
    mediaToAdd.previewURL = mediaDictionary[kMediaPreviewURL];
    mediaToAdd.instagramId = mediaDictionary[kMediaInstagramId];
    mediaToAdd.dateAdded = [NSDate date];
    if (![context save:error]) {
        return NO;
    }
    [self fetchFavorites];
    return YES;
}

- (void)fetchFavorites {
    NSError *error;
    if (![fetchedAllResultsController performFetch:&error]) {
        NSLog(@"error fetching favorites: %@", error.localizedDescription);
    }
    else {
        self.favorites = fetchedAllResultsController.fetchedObjects;
    }
}

- (BOOL)idExistInFavorites:(NSString *)instagramId {
    for (Media *favorite in _favorites) {
        if ([instagramId isEqualToString:favorite.instagramId])
            return YES;
    }
    return NO;
}

- (BOOL)removeFromFavorites:(NSString *)instagramId error:(NSError *__autoreleasing *)error {
    Media *favToDelete;
    for (Media *favorite in _favorites) {
        if ([instagramId isEqualToString:favorite.instagramId]) {
            favToDelete = favorite;
            break;
        }
    }
    if (!favToDelete)
        return NO;
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = delegate.managedObjectContext;
    [context deleteObject:favToDelete];
    if (![context save:error])
        return NO;
    [self fetchFavorites];
    return YES;
}

@end
