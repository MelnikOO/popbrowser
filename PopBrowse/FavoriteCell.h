//
//  FavoriteCell.h
//  PopBrowse
//
//  Created by Олег Мельник on 13.07.15.
//  Copyright (c) 2015 Oleg Melnik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *pictureView;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeLabel;
@property (weak, nonatomic) IBOutlet UIButton *addFavButton;
@property (strong, nonatomic) NSString *instagramId;
- (IBAction)removeFav:(id)sender;

@end
